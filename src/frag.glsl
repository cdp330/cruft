#version 140

in vec2 v_tex_coords;
flat in float v_light_level;
out vec4 color;
uniform sampler2D samp;

void main() {

    vec4 tex = texture(samp, v_tex_coords);
    if(tex.w <= 0.1) {
        discard;
    }

    color = vec4(v_light_level, v_light_level, v_light_level, 1.0) * tex;
}