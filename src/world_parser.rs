use nbt::{CompoundTag, Tag, decode, CompoundTagError};
use std::fs::File;
use std::io::{Read, Cursor, Seek, SeekFrom};
use byteorder::{BigEndian, LittleEndian, ReadBytesExt, ByteOrder};
use std::convert::{TryInto, TryFrom};
use crate::chunk::{Chunk, CHUNK_WIDTH, Block};
use cgmath::{vec3, Vector3};
use nbt::decode::{read_gzip_compound_tag, TagDecodeError};
use std::ops::Deref;

// Using MCRegion spec from minecraft wiki
// TODO: add error types & tests


#[derive(Debug)]
struct ChunkLocation {
    start_sector: u64,
    size_sectors: u64,
}

const REGION_SECTOR_SIZE_BYTES: u64 = 4096;

#[derive(Debug)]
pub enum ChunkReadErr {
    TagExtractErr(String),
    TagDecodeErr(TagDecodeError),
    UnknownCompressionType(u8),
    EndOfFile,
    MetadataParseErr,
    CoordsOutOfRange,
}


impl From<CompoundTagError<'_>> for ChunkReadErr {
    fn from(e: CompoundTagError<'_>) -> Self {
        Self::TagExtractErr(String::from(format!("{:?}", e)))
    }
}

impl From<TagDecodeError> for ChunkReadErr {
    fn from(e: TagDecodeError) -> Self {
        Self::TagDecodeErr(e)
    }
}

pub struct Region {
    data: Cursor<Vec<u8>>
}

impl Region {
    pub fn load(path: &str) -> Self {
        let mut file = File::open(path).unwrap();
        let mut bytes = vec![];
        file.read_to_end(&mut bytes).unwrap();
        Self { data: Cursor::new(bytes) }
    }

    fn get_chunk_location(&mut self, x: i64, z: i64) -> Result<ChunkLocation, std::io::Error> {
        self.data.seek(SeekFrom::Start(Self::chunk_info_location_bytes(x, z)))?;
        let mut buffer = [0u8; 4];
        self.data.read(&mut buffer)?;

        Ok(ChunkLocation {
            start_sector: BigEndian::read_u24(&buffer[..3]) as u64,
            size_sectors: buffer[3] as u64
        })
    }

    // Read a 16x16x16 cubic sub-chunk from the region. Keep in mind that MCRegion chunks are 128 blocks high.
    // Thus, a y-coordinate of 0 will give a cubic sub-chunk from the bottom of the world.
    pub fn read_chunk_cubic(&mut self, chunk_coords: Vector3<i64>) -> Result<Chunk, ChunkReadErr> {
        let location = self.get_chunk_location(chunk_coords.x, chunk_coords.z).map_err(|_| ChunkReadErr::MetadataParseErr)?;
        self.data.seek(SeekFrom::Start(location.start_sector * REGION_SECTOR_SIZE_BYTES)).map_err(|_| ChunkReadErr::EndOfFile)?;

        let chunk_size_bytes = self.data.read_u32::<BigEndian>().map_err(|_| ChunkReadErr::MetadataParseErr)? as usize;
        let compression_type = self.data.read_u8().map_err(|_| ChunkReadErr::MetadataParseErr)?;

        let mut chunk_data = {
            let mut chunk_data = vec![0u8; chunk_size_bytes];
            self.data.read_exact(&mut chunk_data).map_err(|_| ChunkReadErr::EndOfFile)?;
            Cursor::new(chunk_data)
        };

        let chunk_nbt = match compression_type {
            1 => decode::read_gzip_compound_tag(&mut chunk_data)?,
            2 => decode::read_zlib_compound_tag(&mut chunk_data)?,
            _ => Err(ChunkReadErr::UnknownCompressionType(compression_type))?,
        };

        let mut level_data = chunk_nbt.get_compound_tag("Level")?;

        // For a region chunk this will be a 16x128x16 column
        let block_ids: Vec<u8> = level_data.get_i8_vec("Blocks")?.into_iter().map(|b| *b as u8).collect();

        fn get_block(data: &[u8], x: usize, y: usize, z: usize) -> u8 {
            data[y + (z * 128) + (x * 128 * 16)]
        }

        let mut chunk = Chunk::new();
        for x in 0..CHUNK_WIDTH {
            for y in 0..CHUNK_WIDTH {
                for z in 0..CHUNK_WIDTH {
                    chunk.set((x, y, z).into(), match get_block(&block_ids, x, y + 16 * chunk_coords.y as usize, z) {
                        0 => Block::Air,
                        id => {
                            Block::Block{ id: id as u16, light_level: 255 }
                        }
                    });
                }
            }
        }
        chunk.pos = chunk_coords * 16 as i64;
        Ok(chunk)
    }

    fn chunk_info_location_bytes(x: i64, z: i64) -> u64 {
        4 * ((x as u64 % 31) + (z as u64 & 31) * 32)
    }
}
