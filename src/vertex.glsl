#version 150

in vec3 pos;
in vec2 tex_coords;
in float light_level;
out vec2 v_tex_coords;
out float v_light_level;

uniform mat4 mvp;

void main() {
    gl_Position = mvp * vec4(pos, 1.0);
    v_tex_coords = tex_coords;
    v_light_level = light_level;
}