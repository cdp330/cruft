
use cgmath::{Vector3, Basis3, Rotation, Zero, Rad, Deg, Vector2, vec2};
use crate::Vertex;
use std::fmt::{Debug, Formatter};
use cgmath::num_traits::FloatConst;
use crate::assets::SpriteSheetInfo;

type V3f = Vector3<f32>;




#[derive(Copy, Clone, Debug)]
pub enum Dir { Up, Down, Left, Right, Back, Fwd, }



impl Default for Dir {
    fn default() -> Self {Dir::Up}
}

pub const DIRS: [Dir; 6] = [Dir::Up, Dir::Down, Dir::Left, Dir::Right, Dir::Back, Dir::Fwd];
impl Dir {
    pub fn norm_f(&self) -> V3f {
        self.norm().map(|x| x as f32)
    }

    pub fn norm(&self) -> Vector3<isize> {
        match self {
            Dir::Up => (0, 1, 0),
            Dir::Down => (0, -1, 0),
            Dir::Left => (-1, 0, 0),
            Dir::Right => (1, 0, 0),
            Dir::Back => (0, 0, -1),
            Dir::Fwd => (0, 0, 1),
        }.into()
    }

}




///////////////////////////////////////////
// Quad in the "forward" direction
///////////////////////////////////////////
const TILE_W: f32 = 1.0;

pub const QUAD_VERTICES: [Vertex; 4] = [
    Vertex { pos: (0.5, -0.5, 0.5), tex_coords: (TILE_W, 0.0), light_level: 1.0 },
    Vertex { pos: (0.5, 0.5, 0.5), tex_coords: (TILE_W, TILE_W), light_level: 1.0 },
    Vertex { pos: (-0.5, 0.5, 0.5), tex_coords: (0.0, TILE_W), light_level: 1.0 },
    Vertex { pos: (-0.5, -0.5, 0.5), tex_coords: (0.0, 0.0), light_level: 1.0 },
];

pub const QUAD_INDICES: [u32; 6] = [0, 1, 2, 2, 3, 0];
///////////////////////////////////////////





#[derive(Debug)]
pub struct TriMesh { pub vertices: Vec<Vertex>, pub indices: Vec<u32> }

impl TriMesh {
    pub fn extend(mut self, other: Self) -> Self {
        let highest_index = self.vertices.len() as u32;
        self.vertices.extend(other.vertices);
        self.indices.extend(
            other.indices.into_iter().map(|x| x + highest_index)
        );
        self
    }
}

impl From<QuadBundle<'_>> for TriMesh {
    fn from(bundle: QuadBundle<'_>) -> Self {
        let quads:Vec<TriMesh> = bundle.quads.iter().map(|q| q.to_mesh(bundle.sheet_info)).collect();
        quads.into_iter().fold(TriMesh { vertices: vec![], indices: vec![] }, |old, curr| {
            old.extend(curr)
        })
    }
}


pub struct QuadBundle<'a> {
    pub quads: Vec<Quad>,
    pub sheet_info: &'a SpriteSheetInfo,
}

#[derive(Copy, Clone, Debug)]
pub struct Quad {
    pub pos: V3f,
    pub dir: Dir,
    pub light_level: f32,
    pub tex_id: usize
}

impl Quad {
    pub fn with_pos(mut self, pos: V3f) -> Self {
        self.pos = pos;
        self
    }
    pub fn to_mesh(self, spritesheet: &SpriteSheetInfo) -> TriMesh {
        let rotation_axis = match self.dir {
            Dir::Up | Dir::Down => Vector3::unit_x(),
            _ => Vector3::unit_y(),
        };

        let rotation_angle: Rad<_> = Deg(match self.dir {
            Dir::Fwd => 0.0,
            Dir::Right => 90.0,
            Dir::Back => 180.0,
            Dir::Left => 270.0,
            Dir::Up => -90.0,
            Dir::Down => 90.0,
        }).into();

        let rotation: Basis3<f32> = cgmath::Rotation3::from_axis_angle(rotation_axis, rotation_angle);

        let points = QUAD_VERTICES.iter().map(|v| {
            let rotated = rotation.rotate_vector(v.pos.into());

            let mut tex_coords: Vector2<f32> = v.tex_coords.into();
            tex_coords /= spritesheet.sprites_wide as f32;
            tex_coords += spritesheet.get_sprite_coords(self.tex_id).map(|x| x as f32) / spritesheet.sprites_wide as f32;

            Vertex { pos: (rotated + self.pos).into(), tex_coords: tex_coords.into(), light_level: self.light_level }
        }).collect();

        TriMesh { vertices: points, indices: QUAD_INDICES.into() }

    }
}


