use zip::ZipArchive;
use std::fs::File;
use glium::texture::RawImage2d;
use std::io::{Read, Cursor};
use image::{RgbaImage, ImageBuffer, Rgba, RgbImage};
use glium::texture::{SrgbTexture2d};
use cgmath::{Vector2, vec2};


pub struct MinecraftAssets { jar: ZipArchive<File> }
impl MinecraftAssets {
    pub fn new(path: &str) -> Self {
        let jar = File::open(path).unwrap();
        Self { jar: zip::ZipArchive::new(jar).unwrap() }
    }


    pub fn grab_img(&mut self, name: &str) -> RgbaImage {
        let mut buffer = Vec::new();
        self.jar.by_name(name).unwrap().read_to_end(&mut buffer).unwrap();
        let buffer = Cursor::new(buffer);
        image::load(buffer, image::ImageFormat::Png).unwrap().to_rgba()
    }
}

// Combine two images into a larger square spritesheet (don't care about specific order / positions)
pub fn combine_into_spritesheet(left: RgbaImage, right: RgbaImage) -> RgbaImage {
    let new_dimensions = left.width() + right.width();
    let mut new = ImageBuffer::new(new_dimensions, new_dimensions);
    image::imageops::overlay(&mut new, &left, 0, 0);
    image::imageops::overlay(&mut new, &right, left.width(), 0);
    new
}

// Ghetto into implementation
pub trait ImageToGlium {
    fn to_img(self, display: &glium::Display) -> SrgbTexture2d;
}
impl ImageToGlium for RgbaImage {
    fn to_img(self, display: &glium::Display) -> SrgbTexture2d {
        let dimensions = self.dimensions();
        let tex = glium::texture::RawImage2d::from_raw_rgba_reversed(&self.into_raw(), dimensions);
        SrgbTexture2d::new(display, tex).unwrap()
    }
}

// Info about a square spritesheet
pub struct SpriteSheetInfo {
    pub sprite_stride_px: usize,
    pub sprites_wide: usize, // How many sprites in the horizontal direction?
}


impl SpriteSheetInfo {
    // Width and height of sheet.
    pub const fn sheet_width_px(&self) -> usize {
        self.sprites_wide * self.sprite_stride_px
    }

    // Get coordinates (within the sheet) of the nth sprite. Returned coordinates start from top left of sheet
    #[inline]
    pub fn get_sprite_coords(&self, index: usize) -> Vector2<usize> {
        let x = index % self.sprites_wide;
        let y = self.sprites_wide - (index / self.sprites_wide) - 1;
        vec2(x, y)
    }
}
#[cfg(test)]
mod tests {
    use crate::assets::SpriteSheetInfo;
    use cgmath::{Vector2, vec2};

    #[test]
    fn test_indices() {
        let sheet = SpriteSheetInfo {
            sprite_stride_px: 8,
            sprites_wide: 4,
        };
        assert_eq!(sheet.sheet_width_px(), 32);
        assert_eq!(sheet.get_sprite_coords(0), vec2(0, 3));
        assert_eq!(sheet.get_sprite_coords(3), vec2(3, 3));
        assert_eq!(sheet.get_sprite_coords(4), vec2(0, 2));
        assert_eq!(sheet.get_sprite_coords(15), vec2(3, 0));
    }
}