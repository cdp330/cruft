use cgmath::*;
use crate::meshing::Dir;
use std::borrow::Borrow;

type Float = f32;
const CAM_UP: Vector3<Float> = vec3(0.0, 1.0, 0.0);
pub struct Camera {
    pub pos: Point3<Float>,
    pub facing: Vector3<Float>,
    pub velocity: Vector3<Float>,
}

impl Camera {

    pub fn new() -> Self {
        Camera {
            pos: (0.0, 0.0, 20.0).into(),
            facing: (0.0, 0.0, -1.0).into(),
            velocity: Vector3::zero(),
        }
    }

    pub fn get_transform(&self) -> Matrix4<Float> {
        let translation = Matrix4::look_at(self.pos, self.pos + self.facing, Dir::Up.norm_f());
        translation
    }

    pub fn set_vel(&mut self, dir: Option<Dir>) {
        if let Some(dir) = dir {
            self.velocity = match dir {
                Dir::Up => CAM_UP,
                Dir::Down => -CAM_UP,
                Dir::Left => -self.facing.cross(CAM_UP),
                Dir::Right => self.facing.cross(CAM_UP),
                Dir::Back => -self.facing,
                Dir::Fwd => self.facing,
            }
        }
        else {
            self.velocity = Vector3::zero()
        }
        // self.velocity = self.velocity.normalize();
    }

    pub fn set_dir(&mut self, yaw: Rad<Float>, pitch: Rad<Float>) {
        let cam_right = self.facing.cross(CAM_UP); // Perpendicular to the camera's direction
        let rot = Matrix3::from_angle_y(yaw) * Matrix3::from_axis_angle(cam_right, pitch);
        self.facing = rot * self.facing;
        self.facing = self.facing.normalize();
    }

    pub fn update(&mut self) {
        self.pos += self.velocity * 0.05;
    }


}