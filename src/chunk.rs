use crate::meshing::{TriMesh, Quad, DIRS, Dir, QuadBundle};
use cgmath::{Vector3, Zero};
use lazy_static::*;
use crate::assets::SpriteSheetInfo;
// #[allow(unused_mut)]

type V3U = Vector3<usize>;
type V3I = Vector3<isize>;

#[derive(Copy, Clone, Debug)]
pub enum Block {
    Air,
    Block {id: u16, light_level: u8},
}

pub fn block_id_to_tex(id: u16) -> u16 {
    match id {

        13 => 19,
        12 => 18,
        3 => 2,
        7 => 17,
        _ => 1,
    }
}


impl Default for Block {
    fn default() -> Self {
        Self::Air
    }
}

pub const CHUNK_WIDTH : usize = 16;


pub struct Chunk {
    pub pos: Vector3<i64>,
    pub blocks: Box<[Block]>
}

impl Chunk {
    pub fn new() -> Self {
        Self { pos: Vector3::zero(), blocks: vec![Block::default(); usize::pow(CHUNK_WIDTH, 3)].into_boxed_slice() }
    }
}


impl Chunk {
    pub fn get(&self, pos: V3U) -> Block {
        if let Some(index) = Self::index(pos) {
            return self.blocks[index]
        }
        Block::default()
    }

    pub fn set(&mut self, pos: V3U, block: Block) {
        if let Some(index) = Self::index(pos) {
            self.blocks[index] = block;
        }
    }

    const fn index(pos: V3U) -> Option<usize> {
        if pos.x >= CHUNK_WIDTH || pos.y >= CHUNK_WIDTH || pos.z >= CHUNK_WIDTH {
            return None
        }
        Some(pos.x + (pos.y * CHUNK_WIDTH) + (pos.z * CHUNK_WIDTH * CHUNK_WIDTH))
    }

    // Create quads for faces exposed to air
    pub fn build_quads<'a>(&self, sheet_info: &'a SpriteSheetInfo) -> QuadBundle<'a> {

        let mut quads_to_output = vec![];

        for x in 0..CHUNK_WIDTH {
            for y in 0..CHUNK_WIDTH {
                for z in 0..CHUNK_WIDTH {

                    let curr_pos: V3U = (x, y, z).into();

                    if let Block::Block { id, light_level } = self.get(curr_pos) {
                        let curr_pos_i: V3I = curr_pos.map(|x| x as isize);
                        for &dir in DIRS.iter() {

                            let adjacent_pos: V3U = (curr_pos_i + dir.norm())
                                .map(|x| remove_neg(x));

                            let adjacent_tile = self.get(adjacent_pos);
                            if let Block::Air = adjacent_tile {

                                let light_level = (light_level as f32 / 255.0) * match dir {
                                    Dir::Left | Dir:: Right => 0.5,
                                    Dir::Up => 1.0,
                                    Dir::Down => 0.2,
                                    Dir::Fwd | Dir::Back => 0.7,
                                };
                                
                                quads_to_output.push(Quad {
                                    pos: curr_pos.map(|x| x as f32) + self.pos.map(|x| x as f32),
                                    dir,
                                    light_level,
                                    tex_id: block_id_to_tex(id) as usize
                                });
                            }
                        }
                    }

                }
            }
        }
        QuadBundle{ quads: quads_to_output, sheet_info }
    }
}


pub fn remove_neg(x: isize) -> usize {
    if x <= 0 {
        0
    } else {
        x as usize
    }
}

#[cfg(test)]
mod tests {
    use cgmath::Vector3;
    use crate::chunk::*;

    #[test]
    fn test_convert() {
        let v: Vector3<isize> = (-5, -0, 5).into();
        let v: Vector3<usize> = v.map(|x| remove_neg(x) as usize);
        assert_eq!(v, (0, 0, 5).into());
    }

    #[test]
    fn test_chunk() {
        let mut c = Chunk::new();
        c.set((0, 1, 0).into(), Block::Block(0));
        c.set((3, 4, 5).into(), Block::Block(0));
        assert_eq!(c.blocks.len(), 4096);
        assert_eq!(c.get((0, 1, 0).into()), Block::Block(0));
        assert_eq!(c.get((3, 4, 5).into()), Block::Block(0));

        // assert_eq!(c.get((-1, -1, -4).into()), Block::Air);
    }

    #[test]
    fn test_meshing() {
        let mut c = Chunk::new();
        c.set((5, 5, 5).into(), Block::Block(0));

    }
}