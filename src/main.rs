use lazy_static::lazy_static;
use cgmath::{Matrix4, Rad, Vector3, Zero, Vector2, vec3, vec2};
use cgmath::num_traits::FloatConst;
use crate::meshing::{Dir, TriMesh};
use meshing::Quad;
use glium::buffer::BufferType::QueryBuffer;
use crate::chunk::{Chunk, Block};
use glium::backend::glutin::glutin::event::{ModifiersState, MouseScrollDelta, VirtualKeyCode, ElementState, DeviceEvent, KeyboardInput};
use crate::camera::Camera;
use glium::uniforms::*;
use std::fs::File;
use std::io::{Read, BufReader, Cursor};
use zip::ZipArchive;
use zip::read::ZipFile;
use glium::texture::{SrgbTexture2d, RawImage2d};
use assets::ImageToGlium;
use glium::glutin;

mod world_parser;
mod chunk;
mod meshing;
mod camera;
mod assets;
use assets::MinecraftAssets;
mod text_display;
use crate::assets::{combine_into_spritesheet, SpriteSheetInfo};
use image::buffer::ConvertBuffer;
use std::sync::mpsc::channel;
use crate::world_parser::ChunkReadErr;


#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pos: (f32, f32, f32),
    tex_coords: (f32, f32),
    light_level: f32,
}
glium::implement_vertex!(Vertex, pos, tex_coords, light_level);

type MatOutput = [[f32; 4]; 4];


fn main() {
    use glium::glutin;
    use glium::Surface;

    let ev_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new().with_title("test");
    let cb = glutin::ContextBuilder::new().with_depth_buffer(24);
    let display = glium::Display::new(wb, cb, &ev_loop).unwrap();


    let mut asset_bundle = MinecraftAssets::new("minecraft.jar");

    let terrain = asset_bundle.grab_img("terrain.png").to_img(&display);
    let charset = asset_bundle.grab_img("font/default.png").to_img(&display);

    let charset_info = SpriteSheetInfo{ sprite_stride_px: 8, sprites_wide: 16 };
    let terrain_info = SpriteSheetInfo{ sprite_stride_px: 16, sprites_wide: 16 };


    // let chunk = world_parser::Region::load("World/region/r.0.0.mcr").read_chunk_cubic(vec3(0, 0, 1)).unwrap();
    // let mesh: TriMesh = chunk.build_quads(&terrain_info).into();

    // Load a big grid of chunks and put them into a single mesh
    let mut region = world_parser::Region::load("World/region/r.0.0.mcr");
    let mut mesh = TriMesh{ vertices: vec![], indices: vec![] };
    for x in -2..2 {
        for y in 0..8 {
            for z in -2..2 {
                let chunk = match region.read_chunk_cubic(vec3(x, y, z)) {
                    Ok(chunk) => {
                        mesh = mesh.extend(chunk.build_quads(&terrain_info).into());
                        println!("Loaded {:?} successfully", (x, y, z));
                    }
                    Err(e) => {
                        println!("Failed to load {:?}: {:?}", (x, y, z), e);
                    }
                };
            }
        }
    }

    let chunk_vertices = glium::VertexBuffer::new(&display, &mesh.vertices).unwrap();
    let chunk_indices = glium::IndexBuffer::new(&display, glium::index::PrimitiveType::TrianglesList, &mesh.indices).unwrap();
    let program = glium::Program::from_source(
        &display,
        include_str!("vertex.glsl"),
        include_str!("frag.glsl"),
        None,
    ).unwrap();


    let text = text_display::show_text(b"dfjslkaj", &charset_info);
    let text_vertices = glium::VertexBuffer::persistent(&display, &text.vertices).unwrap();
    let text_indices = glium::IndexBuffer::persistent(&display, glium::index::PrimitiveType::TrianglesList,&text.indices).unwrap();


    let params = glium::DrawParameters {
        depth: glium::Depth {
            test: glium::draw_parameters::DepthTest::IfLess,
            write: true,
            ..Default::default()
        },
        blend: glium::draw_parameters::Blend::alpha_blending(),
        backface_culling: glium::draw_parameters::BackfaceCullingMode::CullClockwise,
        ..Default::default()
    };

    let mut cam = camera::Camera::new();


    let text = b"tejalrjfsitgjoiajakljfs0394033, fasdskjasdl";
    let mut index = 0usize;


    ev_loop.run(move |ev, _, control_flow| {
        let mut target = display.draw();
        let (screen_width, screen_height) = target.get_dimensions();

        cam.update();

        let projection_mat: Matrix4<f32> = cgmath::PerspectiveFov {
            fovy: Rad(f32::PI() / 3.0),
            aspect: screen_width as f32 / screen_height as f32,
            near: 0.1,
            far: 1024.0,
        }.into();
        let model_view_projection: MatOutput = (projection_mat * cam.get_transform()).into();

        target.clear_color_and_depth(( 0.5764705, 0.71372, 0.9804, 1.0), 1.0);


        let sampler = Sampler::new(&terrain)
            .magnify_filter(MagnifySamplerFilter::Nearest)
            .minify_filter(MinifySamplerFilter::Nearest);


        target.draw(
            &chunk_vertices,
            &chunk_indices,
            &program,
            &glium::uniform! {mvp: model_view_projection, samp: sampler},
            &params,
        ).unwrap();



        // let model_view_projection: MatOutput = (Matrix4::from_translation(vec3(-1.0, 0.0, 0.0) * Matrix4::from_nonuniform_scale(0.2, 1.0, 1.0))).into();
        let model_view_projection: MatOutput = {
            let t = Matrix4::from_translation(vec3(-0.8, 0.0, 0.0f32));
            let b = Matrix4::from_nonuniform_scale(0.02, 0.06, 1.0);
            (t * b).into()
        };


        let sampler = Sampler::new(&charset)
            .magnify_filter(MagnifySamplerFilter::Nearest)
            .minify_filter(MinifySamplerFilter::NearestMipmapNearest);


        {
            let text = text_display::show_text(b"tejalrjfsitgjoiajakljfs0394033, fasdskjasdl", &charset_info);
            // text_vertices.invalidate();
            // text_indices.invalidate();

            // text_vertices.write(&text.vertices);
            // text_indices.write(&text.indices);
        }
        target.draw(
            &text_vertices,
            &text_indices,
            &program,
            &glium::uniform! {mvp: model_view_projection, samp: sampler},
            &params,
        ).unwrap();


        target.finish().unwrap();


        match ev {
            glutin::event::Event::DeviceEvent { event, ..}
            => if let DeviceEvent::MouseMotion { delta, .. } = event {
                let delta: Vector2<f64> = vec2(-delta.0 / 100.0, -delta.1 / 100.0);
                cam.set_dir(Rad(delta.x as f32), Rad(delta.y as f32));
            },

            glutin::event::Event::WindowEvent { event, .. } => {
                match event {
                    glutin::event::WindowEvent::CloseRequested => {
                        *control_flow = glutin::event_loop::ControlFlow::Exit;
                        return;
                    }

                    glutin::event::WindowEvent::KeyboardInput {input, ..} => match input.state {
                        ElementState::Pressed => if let Some(code) = input.virtual_keycode {
                            cam.set_vel(match code {
                                VirtualKeyCode::Space => Some(Dir::Up),
                                VirtualKeyCode::LShift => Some(Dir::Down),
                                VirtualKeyCode::W => Some(Dir::Fwd),
                                VirtualKeyCode::A => Some(Dir::Left),
                                VirtualKeyCode::S => Some(Dir::Back),
                                VirtualKeyCode::D => Some(Dir::Right),
                                _ => None,
                            });
                        },
                        ElementState::Released => {cam.set_vel(None)}
                    },
                    _ => return,
                }
            }
            _ => {}
        }
    });

    // std::mem::forget(terrain);
}

/*
fn main() {
    let ev_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new().with_title("test");
    let cb = glutin::ContextBuilder::new().with_depth_buffer(24);
    let display = glium::Display::new(wb, cb, &ev_loop).unwrap();


    let mut asset_bundle = MinecraftAssets::new("minecraft.jar");

    // let spritesheet: SrgbTexture2d = combine_into_spritesheet(
    //     asset_bundle.grab_img("terrain.png"),
    //     asset_bundle.grab_img("font/default.png")
    // ).to_img(&display);
    let spritesheet = asset_bundle.grab_img("font/default.png").to_img(&display);

    let quad = text_display::show_quad(b'D');

    let mut vertices = glium::VertexBuffer::dynamic(&display, &quad.vertices).unwrap();
    // let vertices = glium::VertexBuffer::new(&display, &quad.vertices).unwrap();
    let indices = glium::IndexBuffer::new(&display, glium::index::PrimitiveType::TrianglesList, &quad.indices).unwrap();
    let program = glium::Program::from_source(
        &display,
        include_str!("vertex.glsl"),
        include_str!("frag.glsl"),
        None,
    ).unwrap();

    let mut id = b'a';

    let params = glium::DrawParameters {
        depth: glium::Depth {
            test: glium::draw_parameters::DepthTest::IfLess,
            write: true,
            ..Default::default()
        },
        blend: glium::draw_parameters::Blend::alpha_blending(),
        backface_culling: glium::draw_parameters::BackfaceCullingMode::CullClockwise,
        ..Default::default()
    };

    ev_loop.run(move |ev, _, control_flow| {
        let mut target = display.draw();
        target.clear_color_and_depth((0.0, 0.02, 1.0, 1.0), 1.0);

        let sampler = Sampler::new(&spritesheet)
            .magnify_filter(MagnifySamplerFilter::Nearest)
            .minify_filter(MinifySamplerFilter::NearestMipmapLinear);

        let model_view_projection: MatOutput = Matrix4::identity().into();

        target.draw(
            &vertices,
            &indices,
            &program,
            &glium::uniform! {mvp: model_view_projection, samp: sampler},
            &params,
        ).unwrap();
        target.finish().unwrap();

        match ev {
            glutin::event::Event::WindowEvent { event, .. } => {
                match event {
                    glutin::event::WindowEvent::CloseRequested => {
                        *control_flow = glutin::event_loop::ControlFlow::Exit;
                        return;
                    }
                    glutin::event::WindowEvent::KeyboardInput { input, .. } => if let Some(code) = input.virtual_keycode {
                        match code {
                            VirtualKeyCode::F => {
                                vertices.write(&text_display::show_quad(id).vertices);
                                id = id.wrapping_add(1);
                            }
                            _ => {}
                        }
                    },
                    _ => {}
                }
            }
            _ => {}
        }
    });
}*/