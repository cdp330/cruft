use crate::meshing::{TriMesh, Quad, Dir, QuadBundle};
use cgmath::{Vector3, Zero, Vector2, vec2};
use crate::Vertex;
use crate::assets::SpriteSheetInfo;


pub fn show_text(str: &[u8], sheet_info: &SpriteSheetInfo) -> TriMesh {
    let quads = str.iter().enumerate().map(|(i, char) | {
        Quad{ pos: Vector3::unit_x() * i as f32, dir: Dir::Fwd, light_level: 1.0, tex_id: *char as usize }
    }).collect::<Vec<_>>();
    QuadBundle{ quads, sheet_info }.into()
}